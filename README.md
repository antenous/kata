# kata

A project for practicing code katas
https://en.wikipedia.org/wiki/Kata_(programming)

[![pipeline status](https://gitlab.com/antenous/kata/badges/master/pipeline.svg)](https://gitlab.com/antenous/kata/-/commits/master)
[![coverage report](https://gitlab.com/antenous/kata/badges/master/coverage.svg)](https://gitlab.com/antenous/kata/-/commits/master)

## Getting started

The following tools are needed in this the project:
* [CMake](https://cmake.org/) version 3.15 or later
* [gcc](https://gcc.gnu.org/) version 5 or later
* [git](https://git-scm.com/)
* [Make](https://www.gnu.org/software/make/) or [Ninja](https://ninja-build.org/)
* [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) version 14

Tools used to measure code coverage:
* [lcov](http://ltp.sourceforge.net/coverage/lcov.php)
* [gcovr](https://gcovr.com)

Other tools used in this project (fetched at configure time):
* [{fmt}](https://github.com/fmtlib/fmt)
* [GoogleTest](https://github.com/google/googletest)

## Building

These instructions assume that the project has been cloned into a
directory named `kata`. To configure and build the project
run the below commands.

```sh
$ cd kata
$ cmake -S . -B build/
$ cmake --build build/
```

This will create an executable named `kata` in `build/src/`.

Both `GNU` and `Clang` support colored diagnostics but depending on the build
environment the colored output may not be enabled by default. To force colored
diagnostics configure the project with `COLOR_DIAGNOSTICS=On`.

### Building with Docker

Setting up a development environment is sometimes a challenge; the build tools
and libraries may differ between systems, even the underlying OS may not be
compatible. To separate the build process from the build system a [Docker](https://www.docker.com/)
development image is provided. With it the build environment is always the same
regardless of the system around it.

Some IDEs (e.g. [Eclipse](https://www.eclipse.org/)) support building inside a
Docker container. Run the below command to build the image.

```sh
$ cmake --build build/ --target docker-dev-image
```

or

```sh
$ docker build \
  --build-arg USER=`id -un` \
  --build-arg UID=`id -u` \
  --build-arg GID=`id -g` \
  --build-arg TZ=`cat /etc/timezone` \
  -f Dockerfile -t kata:dev .
```

To start the development container run the below command.

```sh
$ docker run \
  --rm -it -h kata-docker \
  -v $PWD:$PWD -v $(ccache -k cache_dir):$HOME/.ccache/ \
  -w $PWD --name kata-dev kata:dev
```

**NOTE!** The Docker commands above may require root permissions

**NOTE!** Depdending on the host build environment the CMake may need to be
reconfigured in the running container

## Running

Running the project executable without any parameters
will print the results of the first kata.

```sh
$ ./build/src/kata
```

## Testing

The unit test framework is the [GoogleTest](https://github.com/google/googletest)
C++ test framework, which is downloaded during the project configuration phase and
built together with the tests. (See User's Guide [here](https://google.github.io/googletest/).)

To run the unit tests build the `check` target.

```sh
$ cmake --build build/ --target check
```

This will build and run the unit tests and show a summary of the results.

### Code coverage

To create a code coverage report, configure the project with
`CODE_COVERAGE=On` and then build with `code-coverage`.

```sh
$ cmake -S . -B build/ -DCODE_COVERAGE=On
$ cmake --build build/ --target check-coverage
```

This will produce the code coverage report in `build/kata-coverage`
folder. View `build/kata-coverage/index.html` to see the results.

**NOTE!** Code coverage will enforce a `Debug` build
