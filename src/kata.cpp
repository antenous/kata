// Copyright (c) 2019 Antero Nousiainen

#include <PrimeFactors.hpp>
#include <fmt/format.h>

int main()
{
    const auto n = 42;
    fmt::print(
        "Prime factors of {1}: {{{0}}}\n", fmt::join(kata::PrimeFactors::generate(n), ","), n);

    return 0;
}
