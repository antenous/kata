// Copyright (c) 2019 Antero Nousiainen

#include "PrimeFactors.hpp"

namespace kata
{

std::vector<int> PrimeFactors::generate(int n)
{
    std::vector<int> primes;

    for (int p(2); p <= n; ++p)
        for (; n % p == 0; n /= p)
            primes.push_back(p);

    return primes;
}

} // namespace kata
