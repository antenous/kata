// Copyright (c) 2019 Antero Nousiainen

#include <PrimeFactors.hpp>
#include <gmock/gmock.h>

namespace kata
{
using namespace testing;

namespace
{
TEST(PrimeFactorsTest, GeneratePrimeFactors)
{
    EXPECT_THAT(PrimeFactors::generate(0), IsEmpty());
    EXPECT_THAT(PrimeFactors::generate(1), IsEmpty());
    EXPECT_THAT(PrimeFactors::generate(2), ElementsAre(2));
    EXPECT_THAT(PrimeFactors::generate(3), ElementsAre(3));
    EXPECT_THAT(PrimeFactors::generate(4), ElementsAre(2, 2));
    EXPECT_THAT(PrimeFactors::generate(5), ElementsAre(5));
    EXPECT_THAT(PrimeFactors::generate(6), ElementsAre(2, 3));
    EXPECT_THAT(PrimeFactors::generate(7), ElementsAre(7));
    EXPECT_THAT(PrimeFactors::generate(8), ElementsAre(2, 2, 2));
    EXPECT_THAT(PrimeFactors::generate(9), ElementsAre(3, 3));
    EXPECT_THAT(PrimeFactors::generate(10), ElementsAre(2, 5));
}
} // namespace
} // namespace kata
