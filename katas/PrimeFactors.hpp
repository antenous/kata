// Copyright (c) 2019 Antero Nousiainen

#ifndef KATAS_PRIMEFACTORS_HPP_
#define KATAS_PRIMEFACTORS_HPP_

#include <vector>

namespace kata
{

class PrimeFactors
{
public:
    static std::vector<int> generate(int n);
};

} // namespace kata

#endif
